SRC = src
#SOURCES = $(shell find -name *.scala)
SOURCES = src/controller/*.scala src/model/*.scala src/view/*.scala src/*.scala
S = scala
SC = scalac
TARGET = bin

compile: $(SOURCES:.scala=.class)

%.class: %.scala
	clear
	@echo ":: Compiling..."
	@echo "Compiling $*.scala.."
	@$(SC) -cp bin -d bin -encoding utf8 $*.scala

run: compile
	@echo ":: Executing..."
	@$(S) -cp bin -encoding utf8 App -feature


clean:
	rm bin/controller/*.class bin/model/*.class bin/view/*.class bin/*.class