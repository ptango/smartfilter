package controller

import model._
import view._

class Filter {

    // Initialisation de la vue et du model
    var model : Model = new Model()
    var view = FilterGui.main(this, this.model)

}