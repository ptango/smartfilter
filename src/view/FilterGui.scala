package view

import controller._
import model._

import scala.swing._
import scala.swing.BorderPanel.Position._
import event._
import java.awt.{ Color, Graphics2D }
import scala.util.Random
import scala.swing.event._

/**
 *
 */
object FilterGui extends SimpleSwingApplication {

    var model : Model = null
    var controller : Filter = null

    /**
     * Main appelé par le controller. Permet de passer le controller et le modèle à la vue
     */
    def main(controller : Filter, model : Model): Unit = {
        this.main(Array())
        this.model = model
        this.controller = controller
    }

    /**
     * Méthode dans laquelle on construit la fenêtre. Elle contient notamment les listeners
     */
    def top = new MainFrame {
        title = "GUI Filter"

        // declare Components here
        val textField = new TextField {
          columns = 30
          text = ""
        }
        contents = textField

        listenTo(textField)
        reactions += {
            case _ => getFilteredWords(textField.text)
        }


    }

    /**
     * Récupère la liste des mots filtré au modèle
     */
    def getFilteredWords(filter: String) = {
        println
        println("Predicate : " + filter)
        var filteredWords = model.getFilteredWords(filter)
        println(filteredWords)
    }
}
