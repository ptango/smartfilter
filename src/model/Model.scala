package model

import controller._
import view._

class Model {

    // Liste de mots à filtrer
    private val list : List[String] = List("bonjour", "docteur", "colis", "coordonnées")

    /**
     * Renvoie la liste des mot filtrés qui respectent le filtre (filter)
     */
    def getFilteredWords(filter: String) : List[String] = {
        var res : List[String] = List()
        def browse(list : List[String]) : Unit = list match  {
            case head :: tail =>
                if(matchs(filter, head)) res ++= List(head)
                browse(tail)
            case Nil =>
        }
        browse(list)
        res
    }

    /**
     * Vérifie si un mot (wordToFilter) correspond au filtre (filter)
     */
    def matchs(filter : String, wordToFilter : String) : Boolean = {
        var index = -1
        def browse(filterCharList: List[Char]) : Boolean = filterCharList match {
            case head :: tail =>
                val indexTemp = findGreaterThan(index, findIndicesOfCharInString(wordToFilter, head))
                if(indexTemp != -1){
                    if(indexTemp > index ){
                        index = indexTemp
                        true && browse(tail)
                    }else{
                        false
                    }
                }else{
                    false
                }
            case Nil => true
        }
        browse(filter.toList)
    }


    /**
     * Trouve tous les indices correspondants à une lettre dans un mot
     */
    def findIndicesOfCharInString(word: String, char: Char) : List[Int] = {
        var res : List[Int] = List()
        var index : Int = 0
        def browse(wordCharList: List[Char]) : Unit = wordCharList match {
            case head :: tail =>
                if(char == head) res ++= List(index)
                index = index + 1
                browse(tail)
            case Nil =>
        }
        browse(word.toList)
        res
    }

    /**
     * Trouve le premier nombre supérieur à index dans une liste. Si trouvé : renvoie le nombre sinon -1
     */
    def findGreaterThan(index: Int, list: List[Int]) : Int = {
        var res = -1
        def browse(list: List[Int]) : Unit = list match {
            case head :: tail =>
                if(head > index) res = head else browse(tail)
            case Nil =>
        }
        browse(list)
        res
    }

}