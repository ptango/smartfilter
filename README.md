Smart Filter
============

J'ai créé cette petite application pour me permettre de perfectionner mon Scala, les concepts de programmation fonctionnelle et le modèle MVC. Cela m'a aussi permis d'entrevoir ScalaSwing et ses *reactions*.

Qu'est ce que c'est ?
---------------------
J'ai décidé de reproduire une feature de l'éditeur SublimeText. Je l'ai appelée *Smart Filter*. L'idée est de permettre le filtrage *"original"* d'une liste.

Prenons un exemple pour expliquer le principe : le mot "coordonnées". Si je décide de prendre le filtre "oroe", ce mot correspond au filtre car :  c **o** o **r** d **o** nné **e** s.

Pour un deuxième exemple, on peut choisir le mot "docteur". Avec le filtre "dt", ça marche ( **d** oc **t** eur). Par contre, avec le filtre "dto", ça ne va par marcher car, il n'y a pas de 'o' dans "docteur" après le 't'.


Comment le faire marcher ?
--------------------------
Pour compiler et/ou exécuter vous pouvez utiliser le makefile. `make` pour compiler et `make run` pour compiler et exécuter.